import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class MainKtTest {
    val testInput = parseInput(
        """
        2199943210
        3987894921
        9856789892
        8767896789
        9899965678
    """.trimIndent()
    )

    @Test
    fun sumLowPoints() {
        assertEquals(15, sumLowPoints(testInput))
    }
}